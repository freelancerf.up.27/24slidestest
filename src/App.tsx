import {AppBar, Box, Button, Container, Toolbar} from '@mui/material';
import React from 'react';
import './App.css';
import {BrowserRouter as Router, Link, Redirect, Route, Switch,} from 'react-router-dom';
import FirstTask from './pages/FirstTask';
import SecondTask from "./pages/SecondTask";
import ThirdTask from './pages/ThirdTask';

function App() {
    return (
        <Router>
            <Box sx={{flexGrow: 1}}>
                <AppBar position="static">
                    <Toolbar>
                        <Button component={Link} to="/first" color="inherit">
                            Task#1
                        </Button>
                        <Button component={Link} to="/second" color="inherit">
                            Task#2
                        </Button>
                        <Button component={Link} to="/third" color="inherit">
                            Task#3
                        </Button>
                    </Toolbar>
                </AppBar>
            </Box>
            <Container maxWidth="lg">
                <Box mt={3}>
                    <Switch>
                        <Route path={`/first`} exact strict>
                            <FirstTask />
                        </Route>
                        <Route path={`/second`} exact strict>
                            <SecondTask />
                        </Route>
                        <Route path={`/third`} exact strict>
                            <ThirdTask />
                        </Route>
                        <Route path="*">
                            <Redirect to={`/first`} />
                        </Route>
                    </Switch>
                </Box>
            </Container>
        </Router>
    );
}

export default App;
