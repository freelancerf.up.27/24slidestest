import useTimer from "../hooks/useTimer"

const FirstTask = () => {
    const timeLeft = useTimer(10)
    return (
        <div>
            Countdown: {timeLeft}
        </div>
    )
}

export default FirstTask
