import countAmoughtValue from "../utils/countAmoughtValue";

const children = {
    value: 1,
    children: [
        {
            value: 0,
            children: [{ value: 1 }, { value: 2, children: [{ value: 12 }] }],
        },
        { value: 14 },
    ],
};

const SecondTask = () => {
    const amount = countAmoughtValue(children)
  return (
      <div>
          Amount of 'Value': {amount}
      </div>
  )
}

export default SecondTask
