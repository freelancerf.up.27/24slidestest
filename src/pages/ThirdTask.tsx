import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const users = [
    {
        id: 1,
        user: {
            name: "Implement Customer",
            hasCredits: false,
            suspended: true,
        },
        slides: 11,
        status: "Open",
        revisions: 0,
        price: "$0",
        manager: "Project manager",
        team: "-",
        deadline: "2021-09-11T00:00:00.000000Z",
        subscribed: true,
    },
    {
        id: 2,
        user: {
            name: "Implement Customer",
            hasCredits: true,
            suspended: false,
        },
        slides: 22,
        status: "New",
        revisions: 0,
        price: "$222",
        manager: "Project manager",
        team: "Alpaca",
        deadline: "2021-09-05T00:00:00.000000Z",
        subscribed: false,
    },
    {
        id: 3,
        user: {
            name: "Implement Customer",
            hasCredits: false,
            suspended: true,
        },
        slides: 33,
        status: "First draft in progress",
        revisions: 0,
        price: "$1000",
        manager: "Project manager",
        team: "Smash",
        deadline: "2021-08-30T00:00:00.000000Z",
        subscribed: false,
    },
    {
        id: 4,
        user: {
            name: "Implement Customer",
            hasCredits: false,
            suspended: false,
        },
        slides: 111,
        status: "Approved",
        revisions: 0,
        price: "$800",
        manager: "Project manager",
        team: "Alpaca",
        deadline: "2021-09-22T00:00:00.000000Z",
        subscribed: true,
    },
    {
        id: 5,
        user: {
            name: "Implement Customer",
            hasCredits: true,
            suspended: false,
        },
        slides: 222,
        status: "First draft in progress",
        revisions: 0,
        price: "$400",
        manager: "Project manager",
        team: "Alpaca",
        deadline: "2021-08-22T00:00:00.000000Z",
        subscribed: false,
    },
];

const isExpired = (date: string): boolean => {
    return new Date() > new Date(date)
}

const ThirdTask = () => {
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>ID</TableCell>
                        <TableCell>Customer</TableCell>
                        <TableCell align="right">Order status</TableCell>
                        <TableCell align="right">Revisions</TableCell>
                        <TableCell align="right">Price</TableCell>
                        <TableCell align="right">PM</TableCell>
                        <TableCell align="right">Design team</TableCell>
                        <TableCell align="right">Deadline</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {users.map((row) => (
                        <TableRow
                            key={row.id}
                            sx={{
                                '&:last-child td, &:last-child th': { border: 0 },
                                backgroundColor: row.user.hasCredits ? 'rgb(129,184,241)' :  row.subscribed ? 'rgb(248,241,147)' : '#fff',
                            }}
                        >
                            <TableCell component="th" scope="row">{row.id}</TableCell>
                            <TableCell component="th" scope="row" sx={{color: row.user.suspended ? 'red' : 'black' }}>{row.user.name}</TableCell>
                            <TableCell align="right">{row.status}</TableCell>
                            <TableCell align="right">{row.revisions}</TableCell>
                            <TableCell align="right">{row.price}</TableCell>
                            <TableCell align="right">{row.manager}</TableCell>
                            <TableCell align="right">{row.team}</TableCell>
                            <TableCell align="right" sx={{
                                color: isExpired(row.deadline) ? 'red' : 'black'
                            }}>{new Date(row.deadline).toLocaleString('en-GB', { timeZone: 'UTC' })}</TableCell>

                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default ThirdTask
