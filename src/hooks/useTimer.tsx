import {useEffect, useState } from "react";

type Time = number;
type UseTimer = (initialTimeSeconds: Time) => Time;
type setTimeoutType = ReturnType<typeof setTimeout>

const useTimer: UseTimer = (initialTimeSeconds: Time) => {
    const [timeLeft, setTimeLeft] = useState(initialTimeSeconds);
    const [intervalId, setIntervalId] = useState<setTimeoutType | null>(null)
    const isTimerEnd = timeLeft <= 0

    const startTimer = () => {
        if (!isTimerEnd && intervalId === null) {
            setIntervalId(setInterval(() => {
                setTimeLeft(timeLeft => timeLeft - 1)
            }, 1000))
        }
    }

    useEffect(() => {
        if (isTimerEnd) {
            clearInterval(intervalId as setTimeoutType)
            setIntervalId(null)
        }
    }, [intervalId, isTimerEnd])

    useEffect(() => {
        return () => {
            clearInterval(intervalId as setTimeoutType)
            setIntervalId(null)
        }
    }, [intervalId])

    startTimer()

    return timeLeft
}

export default useTimer
