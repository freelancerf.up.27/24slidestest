const countValueAmount = (obj: { [index: string]: any }): number => {
    let amount = 0;
    if (obj.value) {
        amount += obj.value
    }
    if (obj.children) {
        obj.children.forEach((item: { [index: string]: any }) => {
            amount += countValueAmount(item)
        })
    }
    return amount
}

export default countValueAmount
